//
//  DateTimeUtils.swift
//  ItunesChallenge
//
//  Created by Osman Avni Koçoğlu on 04.03.19.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import Foundation

class DateTimeUtils {    
    var formatter:DateFormatter?
    let kResponseDateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    let kPrintDateFormat = "dd MMM yyyy"
    
    static let sharedInstance:DateTimeUtils = {
        let instance = DateTimeUtils()
        instance.formatter = DateFormatter()
        return instance
    }()
    
    func dateStringToFormattedString(dateString:String?) -> String {
        guard let dateString = dateString else {
            return ""
        }
        
        DateTimeUtils.sharedInstance.formatter?.dateFormat = kResponseDateFormat
        if let date = DateTimeUtils.sharedInstance.formatter?.date(from: dateString) {
            DateTimeUtils.sharedInstance.formatter?.dateFormat = kPrintDateFormat
            if let formattedDateString = DateTimeUtils.sharedInstance.formatter?.string(from: date) {
                return formattedDateString
            }
        }
        
        return ""
    }
}
