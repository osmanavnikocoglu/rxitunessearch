//
//  VeiwUtility.swift
//  ItunesChallenge
//
//  Created by Osman Avni Koçoğlu on 04.03.19.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//
import UIKit

extension UIView {
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
}
