//
//  ServiceManager.swift
//  ItunesChallenge
//
//  Created by Osman Avni Koçoğlu on 04.03.19.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//
import Foundation
import RxCocoa
import RxSwift

class ServiceManager {
    
    let disposeBag = DisposeBag()
    var searchTextType = "all"
    
    func getStoredTrackList(_ searchText:String) -> Observable<[ResultItem]> {
        guard !searchText.isEmpty, let url = URL(string: "https://itunes.apple.com/search?term=\(searchText)&limit=20&media=\(searchTextType)&country=tr") else {
            return Observable.just([])
        }
        
        return URLSession.shared
            .rx.json(request: URLRequest(url: url))
            .retry(1)
            .map { response in
                
                if let jsonData = try? JSONSerialization.data(withJSONObject:response) {
                    let resultData = try JSONDecoder().decode(Response.self, from: jsonData)
                    
                    if let items = resultData.results {
                        return items
                    }
                }
                
                return []
        }
    }
}
