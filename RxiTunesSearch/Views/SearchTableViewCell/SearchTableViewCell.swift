//
//  SearchTableViewCell.swift
//  RxHayda
//
//  Created by Osman Avni Koçoğlu on 03.03.19.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var readedOverlayView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setReaded(readed:Bool) {
        readedOverlayView.isHidden = !readed
    }
    
    
    

}
