//
//  MainSearchBar.swift
//  RxHayda
//
//  Created by Osman Avni Koçoğlu on 02.03.19.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//


import UIKit

class MainSearchBar: UISearchBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.placeholder = "Please..."
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
