//
//  DetailViews.swift
//  RxHayda
//
//  Created by Osman Avni Koçoğlu on 04.03.19.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import UIKit

class DetailViews: UIView {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var collectionNameLabel: UILabel!
    @IBOutlet weak var subInfo1Label: UILabel!
    @IBOutlet weak var subInfo2Label: UILabel!
    @IBOutlet weak var subInfo3label: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


