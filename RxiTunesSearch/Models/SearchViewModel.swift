//
//  SearchViewModel.swift
//  ItunesChallenge
//
//  Created by Osman Avni Koçoğlu on 04.03.19.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct SearchViewModel {
    
    var searchText = Variable("")
    let disposeBag = DisposeBag()
    
    let apiProvider:ServiceManager
    var data:Driver<[ResultItem]>
    
    init(api: ServiceManager) {
        
        self.apiProvider = api
        
        data = searchText.asObservable()
            .throttle(0.3, scheduler: MainScheduler.instance)
            .filter({$0.count > 3})
            .distinctUntilChanged()
            .flatMapLatest {
                api.getStoredTrackList($0)
            }.asDriver(onErrorJustReturn: [])
        
    }
}
