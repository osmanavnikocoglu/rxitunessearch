//
//  Response.swift
//  ItunesChallenge
//
//  Created by Osman Avni Koçoğlu on 04.03.19.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//


import Foundation

public class Response: JSONSerializable {
    let errorMessage:String?
    let resultCount:Int?
    let results:[ResultItem]?
    
    enum CodingKeys: String, CodingKey {
        case errorMessage = "errorMessage"
        case resultCount = "resultCount"
        case results = "results"
    }
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        errorMessage = try values.decodeIfPresent(String.self, forKey: .errorMessage)
        resultCount = try values.decodeIfPresent(Int.self, forKey: .resultCount)
        results = try values.decodeIfPresent([ResultItem].self, forKey: .results)
    }
}
